package nucleic

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
)

type RNA []Base

func (rna RNA) Len() int           { return len(rna) }
func (rna RNA) Less(i, j int) bool { return rna[i] < rna[j] }
func (rna RNA) Swap(i, j int)      { rna[i], rna[j] = rna[j], rna[i] }

func (rna RNA) At(i int) Base {
	return rna[i]
}

func (rna RNA) Count() map[Base]int {
	counts := map[Base]int{
		Adenine:  0,
		Cytosine: 0,
		Guanine:  0,
		Uracil:   0,
	}

	for i := 0; i < len(rna); i++ {
		if _, ok := counts[rna[i]]; ok {
			counts[rna[i]]++
		}
	}

	return counts
}

func (rna RNA) Differences(other Sequence) ([]int, error) {
	var diffs []int
	if rna.Len() != other.Len() {
		return diffs, errors.New("sequences must have equal length")
	}

	for i := 0; i < rna.Len(); i++ {
		if rna.At(i) != other.At(i) {
			diffs = append(diffs, i)
		}
	}

	return diffs, nil
}

func (rna RNA) Distance(other Sequence) int {
	diffs, err := rna.Differences(other)
	if err != nil {
		return -1
	}

	return len(diffs)
}

func ParseRNA(rd io.Reader) (RNA, error) {
	var rna RNA

	r := bufio.NewReader(rd)

	for {
		b, err := r.ReadByte()
		if err != nil {
			return rna, err
		}

		base := Base(b)
		if !base.IsRNA() {
			return rna, fmt.Errorf("%w: %s cannot be added to RNA", InvalidBase, string(b))
		}

		rna = append(rna, Base(b))
	}
}

func ParseRNAString(s string) (RNA, error) {
	return ParseRNA(bytes.NewBufferString(s))
}
