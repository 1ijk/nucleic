package nucleic

type IUPAC uint8

const (
	Z IUPAC = 1 << iota
	A
	C
	T
	G
	U
)

const EndOfSequence IUPAC = 0

const (
	N = A | C | T | G
	V = A | C | G
	H = A | C | T
	D = A | G | T
	B = C | G | T

	W = A | T
	S = G | C

	M = A | C
	K = G | T

	Y = T | C
	R = G | A
)

var symbols = map[IUPAC]Base{
	Z: 'Z',
	N: 'N',
	A: Adenine,
	C: Cytosine,
	T: Thymine,
	G: Guanine,
	U: Uracil,
	V: 'V',
	H: 'H',
	D: 'D',
	B: 'B',
	W: 'W',
	S: 'S',
	M: 'M',
	K: 'K',
	Y: 'Y',
	R: 'R',
}

var codes = map[Base]IUPAC{
	'Z':  Z,
	'N':  N,
	'A':  Adenine,
	'C':  Cytosine,
	'T':  Thymine,
	'G':  Guanine,
	'U':  Uracil,
	'V':  V,
	'H':  H,
	'D':  D,
	'B':  B,
	'W':  W,
	'S':  S,
	'M':  M,
	'K':  K,
	'Y':  Y,
	'R':  R,
	'\n': EndOfSequence,
}

func (n IUPAC) IsPurine() bool {
	return n.Eql(R)
}

func (n IUPAC) IsPyrimidine() bool {
	return n.Eql(Y)
}

func (n IUPAC) IsKeto() bool {
	return n.Eql(K)
}

func (n IUPAC) IsAmino() bool {
	return n.Eql(M)
}

func (n IUPAC) IsStrong() bool {
	return n.Eql(S)
}

func (n IUPAC) IsWeak() bool {
	return n.Eql(W)
}

func (n IUPAC) Eql(m IUPAC) bool {
	return n == m || n&m != 0
}
