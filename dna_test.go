package nucleic_test

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/1ijk/nucleic"
	"io"
	"testing"
)

// http://rosalind.info/problems/dna/
func TestDNA_Count(t *testing.T) {
	dna, _ := nucleic.ParseDNAString("AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC")

	assert.Equal(t,
		map[nucleic.Base]int{
			nucleic.Adenine:  20,
			nucleic.Cytosine: 12,
			nucleic.Guanine:  17,
			nucleic.Thymine:  21,
		},
		dna.Count(),
	)
}

// http://rosalind.info/problems/rna/
func TestDNA_Transcribe(t *testing.T) {
	dna, _ := nucleic.ParseDNAString("GATGGAACTTGACTACGTAAATT")
	rna, _ := nucleic.ParseRNAString("GAUGGAACUUGACUACGUAAAUU")

	assert.Equal(t, rna, dna.Transcribe())
}

// http://rosalind.info/problems/revc/
func TestDNA_Complement(t *testing.T) {
	dna, _ := nucleic.ParseDNAString("AAAACCCGGT")
	comp, _ := nucleic.ParseDNAString("ACCGGGTTTT")

	res, err := dna.Complement()
	assert.NoError(t, err)

	assert.Equal(t, comp, res)
}

// http://rosalind.info/problems/hamm/
func TestDNA_Distance(t *testing.T) {
	dna1, _ := nucleic.ParseDNAString("GAGCCTACTAACGGGAT")
	dna2, _ := nucleic.ParseDNAString("CATCGTAATGACGGCCT")

	assert.Equal(t, 7, dna1.Distance(dna2))
	assert.Equal(t, 7, dna2.Distance(dna1))

}

func TestParseDNA(t *testing.T) {
	r := bytes.NewBufferString("ACTG")

	dna, err := nucleic.ParseDNA(r)
	assert.Equal(t, io.EOF, err)
	assert.Equal(t,
		nucleic.DNA{nucleic.Adenine, nucleic.Cytosine, nucleic.Thymine, nucleic.Guanine},
		dna,
	)

	r = bytes.NewBufferString("ACUG")

	_, err = nucleic.ParseDNA(r)
	assert.ErrorIs(t, err, nucleic.InvalidBase)
}
