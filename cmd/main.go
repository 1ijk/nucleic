package main

import (
	"gitlab.com/1ijk/nucleic"
	"log"
	"os"
)

var dataType string
var filename string
var function string

func main() {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatalln(err)
	}

	nucleic.ParseDNA(f)
}
