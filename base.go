package nucleic

import "errors"

type Base rune

var InvalidBase = errors.New("invalid base")

const (
	Adenine  = 'A'
	Cytosine = 'C'
	Guanine  = 'G'
	Thymine  = 'T'
	Uracil   = 'U'
)

func (b Base) Byte() byte {
	return byte(b)
}

func (b Base) Complement() (Base, error) {
	switch b {
	case Adenine:
		return Thymine, nil
	case Cytosine:
		return Guanine, nil
	case Guanine:
		return Cytosine, nil
	case Thymine:
		return Adenine, nil
	default:
		return b, InvalidBase
	}
}

func (b Base) IsDNA() bool {
	return b == Thymine ||
		b == Adenine ||
		b == Cytosine ||
		b == Guanine
}

func (b Base) IsRNA() bool {
	return b == Uracil ||
		b == Adenine ||
		b == Cytosine ||
		b == Guanine
}
