package nucleic

import "sort"

type Sequence interface {
	sort.Interface

	// At returns the nucleotide base at the index
	At(int) Base

	From(int) Sequence

	Bytes() []byte

	// Count returns the total count of bases in the sequence
	Count() map[Base]int

	// Difference returns the positions of bases that differ between two sequences
	Differences(Sequence) ([]int, error)

	// Distance returns the Hamming distance between two sequences
	Distance(Sequence) int
}
