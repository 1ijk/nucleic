package nucleic

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
)

type DNA []Base

func (dna DNA) Len() int           { return len(dna) }
func (dna DNA) Less(i, j int) bool { return dna[i] < dna[j] }
func (dna DNA) Swap(i, j int)      { dna[i], dna[j] = dna[j], dna[i] }

func (dna DNA) At(i int) Base {
	return dna[i]
}

func (dna DNA) From(i int) Sequence {
	return dna[i:]
}

func (dna DNA) Bytes() []byte {
	bytes := make([]byte, dna.Len())

	for i := 0; i < dna.Len(); i++ {
		bytes[i] = dna.At(i).Byte()
	}

	return bytes
}

func (dna DNA) Count() map[Base]int {
	counts := map[Base]int{
		Adenine:  0,
		Cytosine: 0,
		Guanine:  0,
		Thymine:  0,
	}

	for i := 0; i < len(dna); i++ {
		if _, ok := counts[dna[i]]; ok {
			counts[dna[i]]++
		}
	}

	return counts
}

func (dna DNA) Differences(other Sequence) ([]int, error) {
	var diffs []int
	if dna.Len() != other.Len() {
		return diffs, errors.New("sequences must have equal length")
	}

	for i := 0; i < dna.Len(); i++ {
		if dna.At(i) != other.At(i) {
			diffs = append(diffs, i)
		}
	}

	return diffs, nil
}

func (dna DNA) Distance(other Sequence) int {
	diffs, err := dna.Differences(other)
	if err != nil {
		return -1
	}

	return len(diffs)
}

func ParseDNA(rd io.Reader) (DNA, error) {
	var dna DNA

	r := bufio.NewReader(rd)

	for {
		b, err := r.ReadByte()
		if err != nil {
			return dna, err
		}

		base := Base(b)
		if !base.IsDNA() {
			return dna, fmt.Errorf("%w: %s cannot be added to DNA", InvalidBase, string(b))
		}

		dna = append(dna, Base(b))
	}
}

func ParseDNAString(s string) (DNA, error) {
	return ParseDNA(bytes.NewBufferString(s))
}

func (dna DNA) Transcribe() RNA {
	rna := make(RNA, dna.Len())

	for i := 0; i < dna.Len(); i++ {
		b := dna[i]

		if b == Thymine {
			b = Uracil
		}

		rna[i] = b
	}

	return rna
}

func (dna DNA) Complement() (DNA, error) {
	var err error

	comp := make(DNA, dna.Len())

	for i, j := 0, dna.Len()-1; i < j; i, j = i+1, j-1 {
		comp[i], err = dna[j].Complement()
		if err != nil {
			return comp, err
		}

		comp[j], err = dna[i].Complement()
		if err != nil {
			return comp, err
		}
	}

	return comp, err
}
